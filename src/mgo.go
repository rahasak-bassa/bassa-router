package main

import (
	"fmt"
	"gitlab.com/rahasaklabs/bassa/spec"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"log"
)

var mSession *mgo.Session

func initmSession() {
	// db info
	//info := &mgo.DialInfo{
	//	Addrs:    []string{mongoConfig.mongoHost},
	//	Database: mongoConfig.mongoDb,
	//	Username: mongoConfig.username,
	//	Password: mongoConfig.password,
	//}
	//s, err := mgo.DialWithInfo(info)

	//log.Printf("INFO connecting mongo with info, %+v", info)

	// connect to mongo
	s, err := mgo.Dial(mongoConfig.mongoHost)
	s.SetMode(mgo.Monotonic, true)
	if err != nil {
		log.Printf("ERROR connecting mongo, %s ", err.Error())
		return
	}
	s.SetMode(mgo.Monotonic, true)

	mSession = s
}

func clearmSession() {
	mSession.Close()
}

func createmDevice(device *spec.Device) {
	sessionCopy := mSession.Copy()
	defer sessionCopy.Close()

	var coll = sessionCopy.DB(mongoConfig.mongoDb).C(mongoConfig.devCol)
	err := coll.Insert(device)
	if err != nil {
		log.Printf("error put device %s", err.Error())
	}
}

func getmDevice(id string) *spec.Device {
	sessionCopy := mSession.Copy()
	defer sessionCopy.Close()

	var coll = sessionCopy.DB(mongoConfig.mongoDb).C(mongoConfig.devCol)
	device := &spec.Device{}
	err := coll.Find(bson.M{"id": id}).One(device)
	if err != nil {
		fmt.Printf("device not found %s", err.Error())
	}

	return device
}

func createmMessage(msg *spec.Message) error {
	sessionCopy := mSession.Copy()
	defer sessionCopy.Close()

	// put
	var coll = sessionCopy.DB(mongoConfig.mongoDb).C(mongoConfig.msgCol)
	err := coll.Insert(msg)
	if err != nil {
		log.Printf("ERROR: fail put message, %s", err.Error())
	}

	return err
}

func getmMessage(uid string) (*spec.Message, error) {
	sessionCopy := mSession.Copy()
	defer sessionCopy.Close()

	// get
	var coll = sessionCopy.DB(mongoConfig.mongoDb).C(mongoConfig.msgCol)
	msg := &spec.Message{}
	err := coll.Find(bson.M{"uid": uid}).One(msg)
	if err != nil {
		log.Printf("ERROR: no message with uid, %s", uid)
	}

	log.Printf("INFO: found message, %+v", msg)

	return msg, err
}

func deletemMessage(uid string) error {
	sessionCopy := mSession.Copy()
	defer sessionCopy.Close()

	// get
	var coll = sessionCopy.DB(mongoConfig.mongoDb).C(mongoConfig.msgCol)
	err := coll.Remove(bson.M{"uid": uid})
	if err != nil {
		log.Printf("ERROR: no message with uid to remove, %s", uid)
	}

	log.Printf("INFO: deleted message with uid, %s", uid)

	return err
}

func updatemMessage(uid string, status string) error {
	sessionCopy := mSession.Copy()
	defer sessionCopy.Close()

	// update msg status
	var coll = sessionCopy.DB(mongoConfig.mongoDb).C(mongoConfig.msgCol)
	err := coll.Update(bson.M{"uid": uid}, bson.M{"$set": bson.M{"status": status}})
	if err != nil {
		log.Printf("ERROR: fail update state uid, %s", uid)
	}

	return err
}

func getmMessages(receiver string) []*spec.Message {
	sessionCopy := mSession.Copy()
	defer sessionCopy.Close()

	// get msgs
	var coll = sessionCopy.DB(mongoConfig.mongoDb).C(mongoConfig.msgCol)
	var msgs []*spec.Message
	err := coll.Find(bson.M{"receiver": receiver}).All(msgs)
	if err != nil {
		log.Printf("ERROR: fail get messages, %s", err.Error())
	}

	log.Printf("INFO: found messages, %+v", msgs)

	return msgs
}

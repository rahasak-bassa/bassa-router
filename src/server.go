package main

import (
	"fmt"
	"log"

	"bytes"
	"encoding/json"
	"gitlab.com/rahasaklabs/bassa/spec"
	"golang.org/x/net/context"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"io/ioutil"
	"net/http"
)

var cons = make(map[string](chan spec.Message))

type Server struct {
}

func (s *Server) RegisterDevice(ctx context.Context, dev *spec.Device) (*spec.Reply, error) {
	log.Printf("INFO: RegisterDevice: %s", dev)

	// get pubkey of sender
	pubkey := getRsaPub(dev.Pubkey)
	if pubkey == nil {
		log.Printf("ERROR: fail to get pubkey of device")
		return &spec.Reply{Uid: dev.Uid, Status: "ERROR"}, nil
	}

	// verify digital signature
	if featureToggleConfig.enableVerifySignature == "yes" {
		err := verifySignature(dev.Token, dev.Digsig, pubkey)
		if err != nil {
			log.Printf("ERROR: fail vefify signature, %s", err)
			return &spec.Reply{Uid: dev.Uid, Status: "ERROR"}, nil
		}
	}

	// create/update device
	err := createDevice(dev)
	if err != nil {
		log.Printf("ERROR: fail create device, %s", err)
		return &spec.Reply{Uid: dev.Uid, Status: "ERROR"}, nil
	}

	log.Printf("INFO: device created")

	return &spec.Reply{Uid: dev.Uid, Status: "OK"}, nil
}

func (s *Server) FindDevice(ctx context.Context, req *spec.DeviceRequest) (*spec.Device, error) {
	log.Printf("INFO: FindDevice: %s", req)

	// get sender device
	sdev, _ := getDevice(req.Sender)
	if sdev == nil {
		log.Printf("ERROR: fail get sender device")
		return nil, status.Errorf(
			codes.InvalidArgument,
			fmt.Sprintf("invalid request"))
	}

	// get pubkey of sender
	pubkey := getRsaPub(sdev.Pubkey)
	if pubkey == nil {
		log.Printf("ERROR: fail to get pubkey of device")
		return nil, status.Errorf(
			codes.InvalidArgument,
			fmt.Sprintf("invalid request"))
	}

	// verify digital signature
	if featureToggleConfig.enableVerifySignature == "yes" {
		err := verifySignature(req.DevId, req.Digsig, pubkey)
		if err != nil {
			log.Printf("ERROR: fail vefify signature, %s", err)
			return nil, status.Errorf(
				codes.InvalidArgument,
				fmt.Sprintf("signature verification fail"))
		}
	}

	// get pubkey of sender
	dev, _ := getDevice(req.DevId)
	if dev == nil {
		log.Printf("ERROR: fail to get device")
		return nil, status.Errorf(
			codes.InvalidArgument,
			fmt.Sprintf("invalid request"))
	}

	return dev, nil
}

func (s *Server) FetchMessages(ctx context.Context, req *spec.FetchRequest) (*spec.FetchResponse, error) {
	log.Printf("INFO: FetchMessages: %s", req)

	// get sender device
	sdev, _ := getDevice(req.Sender)
	if sdev == nil {
		log.Printf("ERROR: fail get sender device")
		return nil, status.Errorf(
			codes.InvalidArgument,
			fmt.Sprintf("invalid request"))
	}

	// get pubkey of sender
	pubkey := getRsaPub(sdev.Pubkey)
	if pubkey == nil {
		log.Printf("ERROR: fail to get pubkey of device")
		return nil, status.Errorf(
			codes.InvalidArgument,
			fmt.Sprintf("invalid request"))
	}

	// verify digital signature
	if featureToggleConfig.enableVerifySignature == "yes" {
		err := verifySignature(req.Sender, req.Digsig, pubkey)
		if err != nil {
			log.Printf("ERROR: fail vefify signature, %s", err)
			return nil, status.Errorf(
				codes.InvalidArgument,
				fmt.Sprintf("signature verification fail"))
		}
	}

	// get messages of sender
	msgs := getMessages(req.Sender)
	resp := &spec.FetchResponse{}
	resp.Uid = req.Uid
	resp.Messages = msgs

	// delete messages
	deleteMessages(req.Sender)

	return resp, nil
}

func (s *Server) SendMessage(ctx context.Context, msg *spec.Message) (*spec.Reply, error) {
	log.Printf("INFO: SendMessage: sender - %s, receiver - %s", msg.Sender, msg.Receiver)

	// get sender device
	sdev, _ := getDevice(msg.Sender)
	if sdev == nil {
		log.Printf("ERROR: fail get sender device")
		return &spec.Reply{Uid: msg.Uid, Status: "ERROR"}, nil
	}

	// get pubkey of sender
	pubkey := getRsaPub(sdev.Pubkey)
	if pubkey == nil {
		log.Printf("ERROR: fail to get pubkey of device")
		return &spec.Reply{Uid: msg.Uid, Status: "ERROR"}, nil
	}

	// verify digital signature
	if featureToggleConfig.enableVerifySignature == "yes" {
		err := verifySignature(msg.Body, msg.Digsig, pubkey)
		if err != nil {
			log.Printf("ERROR: fail vefify signature, %s", err)
			return &spec.Reply{Uid: msg.Uid, Status: "ERROR"}, nil
		}
	}

	// get chanel of receiver and send message
	if c, ok := cons[msg.Receiver]; ok {
		// have client chanel to deliver message
		log.Printf("INFO: have receiver %s to deliver the message, %s", msg.Receiver, msg.Uid)

		c <- *msg
		return &spec.Reply{Uid: msg.Uid, Status: "OK"}, nil
	}

	// came here means no client chanel to deliver message
	// send message via push
	return push(msg)
}

func (s *Server) SendAwa(ctx context.Context, msg *spec.Message) (*spec.Reply, error) {
	log.Printf("INFO: SendAwa: sender - %s", msg.Sender)

	// get sender device
	sdev, _ := getDevice(msg.Sender)
	if sdev == nil {
		log.Printf("ERROR: fail get sender device")
		return &spec.Reply{Uid: msg.Uid, Status: "ERROR"}, nil
	}

	// get pubkey of sender
	pubkey := getRsaPub(sdev.Pubkey)
	if pubkey == nil {
		log.Printf("ERROR: fail to get pubkey of device")
		return &spec.Reply{Uid: msg.Uid, Status: "ERROR"}, nil
	}

	// verify digital signature
	if featureToggleConfig.enableVerifySignature == "yes" {
		err := verifySignature(msg.Body, msg.Digsig, pubkey)
		if err != nil {
			log.Printf("ERROR: fail vefify signature, %s", err)
			return &spec.Reply{Uid: msg.Uid, Status: "ERROR"}, nil
		}
	}

	// delete message with uid
	//deleteMessage(msg.Sender, msg.Uid)

	// get chanel of receiver and send message
	if c, ok := cons[msg.Receiver]; ok {
		// have client chanel to deliver message
		log.Printf("INFO: have receiver %s to deliver the awa message, %s", msg.Receiver, msg.Uid)

		c <- *msg
		return &spec.Reply{Uid: msg.Uid, Status: "OK"}, nil
	}

	// came here means no client chanel to deliver message
	// send message via push
	return push(msg)
}

func (s *Server) StreamMessage(connect *spec.Connect, stream spec.BassaService_StreamMessageServer) error {
	log.Printf("INFO: StreamMessage: %s", connect)

	// identify client id
	cid := connect.Sender

	// chanel with  buffer of 10 messages
	// this chanel can used by other peers to send messages to the client
	c := make(chan spec.Message, 10)
	cons[cid] = c

	// send incoming messages to client
listener:
	for {
		select {
		case m := <-c:
			log.Printf("INFO: send message")

			// forward message
			err := stream.Send(&m)
			if err != nil {
				log.Printf("ERROR: fail send message, notify via push notification %s", err)

				// send message via push notification
				push(&m)

				break listener
			}
		}
	}

	// remove chanel from cons
	log.Printf("INFO: remove client chanel from cons, %s", cid)
	delete(cons, cid)

	return nil
}

func push(msg *spec.Message) (*spec.Reply, error) {
	// came here means no client chanel to deliver message, so deliver via push
	// save message
	createMessage(msg)

	// try to deliver via push notification
	rdev, _ := getDevice(msg.Receiver)
	if rdev == nil {
		log.Printf("ERROR: no receiver device to send notificaation")
		return &spec.Reply{Uid: msg.Uid, Status: "ERROR"}, nil
	}

	// send via notification
	// notification body
	msgMap := make(map[string]string)
	msgMap["sender"] = msg.Sender
	msgMap["uid"] = msg.Uid
	msgMap["body"] = msg.Body
	msgMap["type"] = msg.Type
	mBytes, _ := json.Marshal(msgMap)

	// send push notification
	reqMap := make(map[string]string)
	reqMap["deviceType"] = rdev.Type
	reqMap["deviceToken"] = rdev.Token
	reqMap["title"] = msg.Sender
	reqMap["message"] = string(mBytes)

	// body based on message type
	if msg.Type == "SELFIE_CALL" {
		reqMap["body"] = "Selfie call received"
	} else if msg.Type == "SELFIE" {
		reqMap["body"] = "Selfie received"
	} else if msg.Type == "FRIEND_REQUEST" {
		reqMap["body"] = "New friend request received"
	} else if msg.Type == "ACCEPT_REQUEST" {
		reqMap["body"] = "Friend request accepted"
	} else {
		reqMap["body"] = "New message received"
	}

	rBytes, _ := json.Marshal(reqMap)
	post(rBytes)

	log.Printf("INFO: sent message %s to reciever %s via notification", msg.Type, msg.Receiver)

	// TODO check and return error
	return &spec.Reply{Uid: msg.Uid, Status: "OK"}, nil
}

func post(data []byte) (string, int) {
	log.Printf("INFO: send request to, %s with %s", apiConfig.notificationApi, string(data))

	// request
	req, err := http.NewRequest("POST", apiConfig.notificationApi, bytes.NewBuffer(data))
	if err != nil {
		log.Printf("ERROR: fail init request, %s", err.Error)
		return "error request", 400
	}

	// headers
	req.Header.Add("Content-Type", "application/json")

	// send request
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Printf("ERROR: fail send request, %s", err.Error())
		return "error request", 400
	}
	defer resp.Body.Close()
	b, _ := ioutil.ReadAll(resp.Body)

	// TODO parse response and check success=1

	log.Printf("INFO: got response %s, status %d", string(b), resp.StatusCode)

	return string(b), resp.StatusCode
}

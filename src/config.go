package main

import (
	"os"
)

type Config struct {
	serviceName string
	dotLogs     string
}

type ApiConfig struct {
	notificationApi string
}

type CassandraConfig struct {
	host        string
	port        string
	keyspace    string
	consistancy string
}

type MongoConfig struct {
	mongoHost string
	mongoPort string
	mongoDb   string
	username  string
	password  string
	devCol    string
	msgCol    string
}

type FeatureToggleConfig struct {
	enableVerifySignature string
}

var config = Config{
	serviceName: getEnv("SERVICE_NAME", "router"),
	dotLogs:     getEnv("DOT_LOGS", ".logs"),
}

var apiConfig = ApiConfig{
	notificationApi: getEnv("NOTIFICATION_API", "http://localhost:8762/api/notifications"),
}

var cassandraConfig = CassandraConfig{
	host:        getEnv("CASSANDRA_HOST", "dev.localhost"),
	port:        getEnv("CASSANDRA_PORT", "9042"),
	keyspace:    getEnv("CASSANDRA_KEYSPACE", "bassa"),
	consistancy: getEnv("CASSANDRA_CONSISTANCY", "LOCAL_QUORUM"),
}

var mongoConfig = MongoConfig{
	mongoHost: getEnv("MONGO_HOST", "dev.localhost"),
	mongoPort: getEnv("MONGO_PORT", "27017"),
	mongoDb:   getEnv("MONGO_DB", "bassa"),
	username:  getEnv("MONGO_USER", "admin"),
	password:  getEnv("MONGO_PASS", "admin"),
	devCol:    getEnv("MONGO_DEV_COLLECTION", "devs"),
	msgCol:    getEnv("MONGO_MSG_COLLECTION", "msgs"),
}

var featureToggleConfig = FeatureToggleConfig{
	enableVerifySignature: getEnv("ENABLE_VERIFY_SIGNATURE", "no"),
}

func getEnv(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}

	return fallback
}

package main

import (
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"encoding/asn1"
	"encoding/base64"
	"log"
	"strings"
)

var keySize = 1024

func getRsaPub(keyStr string) *rsa.PublicKey {
	// key is base64 encoded
	data, err := base64.StdEncoding.DecodeString(keyStr)
	if err != nil {
		log.Printf("ERROR: fail get senzie rsapub, %s", err.Error())
		return nil
	}

	// this for ios key
	var pubKey rsa.PublicKey
	if rest, err := asn1.Unmarshal(data, &pubKey); err != nil {
		log.Printf("INFO: not ios key", keyStr)
	} else if len(rest) != 0 {
		log.Printf("INFO: not ios key, invalid lenght, %s", keyStr)
	} else {
		return &pubKey
	}

	// this is for android
	// get rsa public key
	pub, err := x509.ParsePKIXPublicKey(data)
	if err != nil {
		log.Printf("INFO: not android key, %s", keyStr)
		return nil
	}
	switch pub := pub.(type) {
	case *rsa.PublicKey:
		return pub
	default:
		return nil
	}

	return nil
}

func sign(payload string, key *rsa.PrivateKey) (string, error) {
	// remove unwated characters and get sha256 hash of the payload
	hashed := sha256.Sum256([]byte(payload))

	// sign the hased payload
	signature, err := rsa.SignPKCS1v15(rand.Reader, key, crypto.SHA256, hashed[:])
	if err != nil {
		log.Printf("ERROR: fail to sign, %s", err.Error())
		return "", err
	}

	// reutrn base64 encoded string
	return base64.StdEncoding.EncodeToString(signature), nil
}

func verifySignature(payload string, signature64 string, key *rsa.PublicKey) error {
	// decode base64 encoded signature
	signature, err := base64.StdEncoding.DecodeString(signature64)
	if err != nil {
		log.Printf("ERROR: fail to base64 decode, %s", err.Error())
		return err
	}

	// remove unwated characters and get sha256 hash of the payload
	replacer := strings.NewReplacer("\n", "", "\r", "", " ", "")
	fpayload := strings.TrimSpace(strings.ToLower(replacer.Replace(payload)))
	hashed := sha256.Sum256([]byte(fpayload))

	return rsa.VerifyPKCS1v15(key, crypto.SHA256, hashed[:], signature)
}

package main

import (
	"log"
	"net"
	"time"

	"gitlab.com/rahasaklabs/bassa/spec"
	"google.golang.org/grpc"
	"google.golang.org/grpc/keepalive"
	"google.golang.org/grpc/reflection"
)

func main() {
	// setup logging first
	initLogger()

	// init cassandra
	initSession()
	bootstrap()

	// init mongo
	//initmSession()

	lis, err := net.Listen("tcp", ":9000")
	if err != nil {
		log.Printf("ERROR: failed to listen: %s", err)
	}

	var kaep = keepalive.EnforcementPolicy{
		//MinTime:             5 * time.Second, // If a client pings more than once every 5 seconds, terminate the connection
		PermitWithoutStream: true, // Allow pings even when there are no active streams
	}

	var kasp = keepalive.ServerParameters{
		//MaxConnectionIdle:     15 * time.Second, // If a client is idle for 15 seconds, send a GOAWAY
		//MaxConnectionAge:      30 * time.Second, // If any connection is alive for more than 30 seconds, send a GOAWAY
		MaxConnectionAgeGrace: 10 * time.Second, // Allow 5 seconds for pending RPCs to complete before forcibly closing connections
		Time:                  10 * time.Second, // Ping the client if it is idle for 5 seconds to ensure the connection is still active
		Timeout:               5 * time.Second,  // Wait 1 second for the ping ack before assuming the connection is dead
	}

	// register server
	server := Server{}
	grpcServer := grpc.NewServer(grpc.KeepaliveEnforcementPolicy(kaep), grpc.KeepaliveParams(kasp))
	spec.RegisterBassaServiceServer(grpcServer, &server)

	// register the reflection service which allows clients to determine the methods
	// for this gRPC service
	reflection.Register(grpcServer)

	log.Printf("INFO: starting service")

	// start to serve
	err = grpcServer.Serve(lis)
	if err != nil {
		log.Fatalf("failed to serve: %s", err)
	}
}

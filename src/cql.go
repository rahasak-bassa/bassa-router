package main

import (
	"errors"
	"log"
	"os"
	"sort"
	"strconv"
	"time"

	"github.com/gocql/gocql"
	"gitlab.com/rahasaklabs/bassa/spec"
)

var Session *gocql.Session

func initSession() {
	port := func(p string) int {
		i, err := strconv.Atoi(p)
		if err != nil {
			return 9042
		}

		return i
	}

	consistancy := func(c string) gocql.Consistency {
		gc, err := gocql.MustParseConsistency(c)
		if err != nil {
			return gocql.All
		}

		return gc
	}

	cluster := gocql.NewCluster(cassandraConfig.host)
	cluster.Port = port(cassandraConfig.port)
	//cluster.Keyspace = cassandraConfig.keyspace
	cluster.Consistency = consistancy(cassandraConfig.consistancy)
	cluster.ConnectTimeout = time.Second * 10

	s, err := cluster.CreateSession()
	if err != nil {
		log.Printf("ERROR: fail create cassandra session, %s", err.Error())
		os.Exit(1)
	}
	Session = s
}

func bootstrap() {
	// create tables
	q1 := `
		CREATE KEYSPACE IF NOT EXISTS bassa
		WITH replication = {
			'class':'NetworkTopologyStrategy',
			'DC1':'1'
		};
	`
	q2 := `
		CREATE TABLE IF NOT EXISTS bassa.devices (
			id TEXT,
			typ TEXT,
			tkn TEXT,
			pubkey TEXT,
			timestamp TIMESTAMP,
			PRIMARY KEY(id)
		);
    `
	q3 := `
		CREATE TABLE IF NOT EXISTS bassa.messages (
			uid TEXT,
			sender TEXT,
			receiver TEXT,
			typ TEXT,
			body TEXT,
			digsig TEXT,
			timestamp TIMESTAMP,
			PRIMARY KEY(receiver, uid)
		);
    `

	err := Session.Query(q1).Exec()
	if err != nil {
		log.Printf("ERROR: fail create keyspace, %s", err.Error())
		os.Exit(1)
	}

	err = Session.Query(q2).Exec()
	if err != nil {
		log.Printf("ERROR: fail create devices table, %s", err.Error())
		os.Exit(1)
	}

	err = Session.Query(q3).Exec()
	if err != nil {
		log.Printf("ERROR: fail create messages table, %s", err.Error())
		os.Exit(1)
	}
}

func clearSession() {
	Session.Close()
}

func createDevice(device *spec.Device) error {
	q := `
        INSERT INTO bassa.devices (
            id,
            typ,
			tkn,
			pubkey,
			timestamp
        )
        VALUES (?, ?, ?, ?, ?)
    `
	err := Session.Query(q,
		device.Id,
		device.Type,
		device.Token,
		device.Pubkey,
		time.Now()).Exec()
	if err != nil {
		log.Printf("ERROR: fail create device, %s", err.Error())
	}

	return err
}

func getDevice(id string) (*spec.Device, error) {
	m := map[string]interface{}{}
	q := `
        SELECT * FROM bassa.devices
		WHERE id = ?
        LIMIT 1
    `
	itr := Session.Query(q, id).Consistency(gocql.One).Iter()
	for itr.MapScan(m) {
		device := &spec.Device{}
		device.Id = m["id"].(string)
		device.Type = m["typ"].(string)
		device.Token = m["tkn"].(string)
		device.Pubkey = m["pubkey"].(string)

		log.Printf("INFO: found device, %v", device)

		return device, nil
	}

	return nil, errors.New("device not found")
}

func updateDevice(id string, token string) error {
	q := `
        UPDATE bassa.deivces
		SET token = ?
		WHERE id = ?
    `
	err := Session.Query(q, token, id).Exec()
	if err != nil {
		log.Printf("ERROR: fail update device, %s", err.Error())
		return err
	}

	return nil
}

func createMessage(message *spec.Message) error {
	q := `
        INSERT INTO bassa.messages (
            uid,
            sender,
            receiver,
            typ,
			body,
			digsig,
			timestamp
        )
        VALUES (?, ?, ?, ?, ?, ?, ?)
    `
	err := Session.Query(q,
		message.Uid,
		message.Sender,
		message.Receiver,
		message.Type,
		message.Body,
		message.Digsig,
		time.Now()).Exec()
	if err != nil {
		log.Printf("ERROR: fail create message, %s", err.Error())
	}

	return err
}

func getMessages(receiver string) []*spec.Message {
	m := map[string]interface{}{}
	q := `
        SELECT * FROM bassa.messages
		WHERE receiver = ?
    `
	var messages []*spec.Message
	itr := Session.Query(q, receiver).Consistency(gocql.One).Iter()
	for itr.MapScan(m) {
		message := &spec.Message{}
		message.Uid = m["uid"].(string)
		message.Receiver = m["receiver"].(string)
		message.Sender = m["sender"].(string)
		message.Type = m["typ"].(string)
		message.Body = m["body"].(string)
		message.Digsig = m["digsig"].(string)
		timestamp := m["timestamp"].(time.Time)
		if !timestamp.IsZero() {
			message.Timestamp = timestamp.UnixNano() / int64(time.Millisecond)
		}

		// apend to array
		messages = append(messages, message)

		m = map[string]interface{}{}
	}

	log.Printf("INFO: found messages, %v", messages)

	// sort messages with timestamp order
	sort.Slice(messages, func(i, j int) bool {
		return messages[j].Timestamp < messages[i].Timestamp
	})

	return messages
}

func deleteMessages(receiver string) error {
	q := `
        DELETE FROM bassa.messages
		WHERE receiver = ?
    `
	err := Session.Query(q, receiver).Exec()
	if err != nil {
		log.Printf("ERROR: fail delete messages, %s", err.Error())
	}

	return err
}

func deleteMessage(receiver string, uid string) error {
	q := `
        DELETE FROM bassa.messages
		WHERE receiver = ? AND uid = ?
    `
	err := Session.Query(q, receiver, uid).Exec()
	if err != nil {
		log.Printf("ERROR: fail delete message, %s", err.Error())
	}

	return err
}
